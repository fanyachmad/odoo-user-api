from odoo import api, models, fields
from odoo.exceptions import ValidationError, UserError

class MaterialTest(models.Model):
    _name = 'user.test'
    _description = 'User Test'

    userid = fields.Integer("User Id")
    name = fields.Char(string='Nama Lengkap')
    username = fields.Char(String="Username")
    passwd = fields.Char(String="Password")
    status = fields.Char(String="Status")
    state = fields.Selection([('draft', 'Draft'), ('confirm', 'Confirmed'), ('cancel', 'Cancelled')], string='Status',
                             default='draft')

    def name_get(self):
        result = []
        for mt in self:
            result.append((mt.id, "%s" % (mt.name or '')))
        return result

    def action_button_draft(self):
        if self.filtered(lambda inv: inv.state not in ('cancel')):
            raise UserError(('Plan must be on cancel state '))
        return self.write({'state': 'draft'})

    def action_button_confirm(self):
        if self.filtered(lambda inv: inv.state not in ('draft')):
            raise UserError(('Plan must be on draft state '))
        return self.write({'state': 'confirm'})

    def action_button_cancel(self):
        if self.filtered(lambda inv: inv.state not in ('confirm')):
            raise UserError(('Plan must be on confirmed state '))
        return self.write({'state': 'cancel'})
