import json
import werkzeug.wrappers
from odoo import http, _, exceptions

class UserTestRestApi(http.Controller):

    #View semua user (api-akses : localhost:<port>/api/material_all/)
    @http.route('/api/user_all/', auth='public', csrf=False, type='http', methods=['GET'])
    def view_material_all(self, **params):
        user = http.request.env['user.test'].sudo().search([])
        dict_material = {}
        data_user = []
        for us in user:
            dict_material = {
                'id': us.userid,
                'nama': us.name,
                'username': us.username,
                'password': us.passwd,
                'status': us.status,
            }
            data_user.append(dict_material)
        data = {
            'status': 200,
            'message': 'success',
            'response': data_user
        }
        try:
            return werkzeug.wrappers.Response(
                status=200,
                content_type='application/json; charset=utf-8',
                response=json.dumps(data)
            )
        except:
            return werkzeug.wrappers.Response(
                status=400,
                content_type='application/json; charset=utf-8',
                headers=[('Access-Control-Allow-Origin', '*')],
                response=json.dumps({
                    'error': 'Error',
                    'error_descrip': 'Error Description',
                })
            )

    # View material filter id (api-akses : localhost:<port>/api/material_all/?type=cotton)
    @http.route('/api/user/', auth='public' ,csrf=False, type='http', methods=['GET'])
    def view_material_type(self, **params):
        get_type = params.get("id")
        user = http.request.env['user.test'].sudo().search([('userid', '=', get_type)])
        dict_material = {}
        data_user = []
        for us in user:
            dict_material = {
                'id': us.userid,
                'nama': us.name,
                'username': us.username,
                'password': us.passwd,
                'status': us.status,
            }
            data_user.append(dict_material)
        data = {
            'status': 200,
            'message': 'success',
            'response': data_user
        }
        try:
            return werkzeug.wrappers.Response(
                status=200,
                content_type='application/json; charset=utf-8',
                response=json.dumps(data)
            )
        except:
            return werkzeug.wrappers.Response(
                status=400,
                content_type='application/json; charset=utf-8',
                headers=[('Access-Control-Allow-Origin', '*')],
                response=json.dumps({
                    'error': 'Error',
                    'error_descrip': 'Error Description',
                })
            )

    # Hapus user (api-akses : localhost:<port>/api/delete_user/?code=<value material_code>)
    @http.route('/api/delete_user/', auth='public', type='http', methods=['DELETE'], csrf=False)
    def del_material(self, **params):
        get_id = params.get("id")
        material = http.request.env['user.test'].sudo().search([('userid', '=', get_id)])
        if material:
            material.unlink()
            try:
                return werkzeug.wrappers.Response(
                    status=200,
                    content_type='application/json; charset=utf-8',
                    response=json.dumps({
                        'message' : "Material Code %s successfully deleted" % (get_id), "delete": True
                    })
                )
            except:
                return werkzeug.wrappers.Response(
                    status=400,
                    content_type='application/json; charset=utf-8',
                    headers=[('Access-Control-Allow-Origin', '*')],
                    response=json.dumps({
                        'error': 'Error',
                        'error_descrip': 'Error Description',
                    })
                )

